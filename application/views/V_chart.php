
    <!-- Chart -->
    <!-- <div class="row">
        <div class="col-12"> -->
        <h4>
            Nama : Dinda Dewani Khairunnisa Rahardyani
            <br>
            NIM : 1900016158
        </h4>    
        
        <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h1 class="m-0 font-weight-bold text-primary">Statistik Data Per Bulan</h1>
                </div>
                <!-- Card Body -->
                <!-- data chart surat masuk -->
                <?php foreach ($rekapdata as $v => $d){
                    $arrProd[] = ['label' => $d->nama_bulan, 'y' => $d->jumlah_bulanan]; 
                }
                // print_r(json_encode($arrProd, JSON_NUMERIC_CHECK));die();
                ?>

                <!-- data chart surat keluar -->
                <?php foreach ($rekapdataklr as $v => $d){
                    $arrProdKlr[] = ['label' => $d->nama_bulan, 'y' => $d->jml_bulanan]; 
                }
                // print_r(json_encode($arrProdKlr, JSON_NUMERIC_CHECK));die();
                ?>

                <?php foreach ($rekapdata as $v => $d){
                            $arrProdLine[] = ['x' => $d->nama_bulan, 'y' => $d->jumlah_bulanan]; 
                }?>
                <?php foreach ($rekapdataklr as $v => $d){
                    $arrProdLineKlr[] = ['x' => $d->nama_bulan, 'y' => $d->jml_bulanan]; 
                }?>

                <!DOCTYPE HTML>
                <html>
                <head>  
                <script>
                window.onload = function () {

                var chart = new CanvasJS.Chart("chartBar", {
                    animationEnabled: true,
                    title:{
                        text: ""
                    },	
                    axisY: {
                        title: "Jumlah Surat Masuk",
                        titleFontColor: "#4F81BC",
                        lineColor: "#4F81BC",
                        labelFontColor: "#4F81BC",
                        tickColor: "#4F81BC"
                    },
                    axisY2: {
                        title: "Jumlah Surat Terkirim",
                        titleFontColor: "#C0504E",
                        lineColor: "#C0504E",
                        labelFontColor: "#C0504E",
                        tickColor: "#C0504E"
                    },	
                    toolTip: {
                        shared: true
                    },
                    legend: {
                        cursor:"pointer",
                        itemclick: toggleDataSeries
                    },
                    data: [{
                        type: "column",
                        name: "Surat Masuk",
                        legendText: "Surat Masuk",
                        showInLegend: true, 
                        // dataPoints:[
                        //     { label: "Saudi", y: 266.21 },
                        //     { label: "Venezuela", y: 302.25 },
                        //     { label: "Iran", y: 157.20 },
                        //     { label: "Iraq", y: 148.77 },
                        //     { label: "Kuwait", y: 101.50 },
                        //     { label: "UAE", y: 97.8 }
                        // ]
                        dataPoints:
                            <?php echo json_encode($arrProd, JSON_NUMERIC_CHECK); ?>
                        
                    },
                    {
                        type: "column",	
                        name: "Surat Keluar",
                        legendText: "Surat Keluar",
                        axisYType: "secondary",
                        showInLegend: true,
                        dataPoints:
                            <?php echo json_encode($arrProdKlr, JSON_NUMERIC_CHECK); ?>
                    }]
                });
                chart.render();

                function toggleDataSeries(e) {
                    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    }
                    else {
                        e.dataSeries.visible = true;
                    }
                    chart.render();
                }
                }
                </script>
                </head>
                <body>
                <div id="chartBar" style="height: 300px; width: 100%;"></div>
                <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                </body>
                </html>
                
            </div>
        <!-- </div>
    </div> -->


   