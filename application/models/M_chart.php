<?php
class M_chart extends CI_Model{

    public function getDataCharts()
    {
        $tahun = date("Y");
        $query = "SELECT id_bulan, nama_bulan, jumlah_bulanan FROM bulan
        LEFT JOIN (
            SELECT MONTH(tanggal_masuk) AS bulan, COUNT(*) AS jumlah_bulanan FROM tb_srt_masuk
            WHERE YEAR(tanggal_masuk) = '$tahun'
            GROUP BY MONTH(tanggal_masuk)
        ) msk ON (bulan.id_bulan = msk.bulan)";

        return $this->db->query($query);
    }

    public function getDataChartKlr()
    {
        $tahun = date("Y");
        $query = "SELECT id_bulan, nama_bulan, jml_bulanan FROM bulan
        LEFT JOIN (
            SELECT MONTH(tanggal_kirim) AS bulan, COUNT(*) AS jml_bulanan FROM tb_srt_keluar
            WHERE YEAR(tanggal_kirim) = '$tahun'
            GROUP BY MONTH(tanggal_kirim)
        ) klr ON (bulan.id_bulan = klr.bulan)";

        return $this->db->query($query);
    }
}
?>