<?php

class Chart extends CI_Controller{
       
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_chart');
    }

    public function index()
    {
        $data['rekapdata'] = $this->M_chart->getDataCharts()->result();
        $data['rekapdataklr'] = $this->M_chart->getDataChartKlr()->result();
        $this->load->view('v_chart', $data);
    }
}
?>