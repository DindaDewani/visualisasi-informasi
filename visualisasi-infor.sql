-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Jul 2022 pada 14.21
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `visualisasi-infor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bulan`
--

CREATE TABLE `bulan` (
  `id_bulan` int(11) NOT NULL,
  `nama_bulan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bulan`
--

INSERT INTO `bulan` (`id_bulan`, `nama_bulan`) VALUES
(1, 'Januari'),
(2, 'Februari'),
(3, 'Maret'),
(4, 'April'),
(5, 'Mei'),
(6, 'Juni'),
(7, 'Juli'),
(8, 'Agustus'),
(9, 'September'),
(10, 'Oktober'),
(11, 'November'),
(12, 'Desember');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_srt_keluar`
--

CREATE TABLE `tb_srt_keluar` (
  `id_keluar` int(11) NOT NULL,
  `tanggal_surat_keluar` date NOT NULL,
  `nomor_surat_keluar` varchar(100) NOT NULL,
  `bidang_pengolah` varchar(100) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `tanggal_kirim` date NOT NULL,
  `tembilan` varchar(100) NOT NULL,
  `file_keluar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_srt_keluar`
--

INSERT INTO `tb_srt_keluar` (`id_keluar`, `tanggal_surat_keluar`, `nomor_surat_keluar`, `bidang_pengolah`, `perihal`, `tujuan`, `tanggal_kirim`, `tembilan`, `file_keluar`) VALUES
(1, '2022-01-02', '2345', 'sd', 'dgh', 'dfgh', '2022-01-10', 'tyu', 'ghj'),
(2, '2022-02-14', 'df', 'tyui', 'tui', 'ghjk', '2022-02-22', 'hkjl', 'srt'),
(3, '2022-02-15', 'fdghj', 'rtyg', 'rty', 'fgh', '2022-02-22', 'gfhj', 'h'),
(4, '2022-03-08', 'vbn', 'asg', 'fgh', 'cxvb', '2022-03-15', 'dhj', 'fg'),
(5, '2022-12-01', '56', 'dfgh', 'fgh', 'cvb', '2022-12-19', 'asdfgh', 'cvbn'),
(6, '2022-12-01', '789', 'fghj', 'cvbn', 'gh', '2022-12-20', 'hj', 'rty'),
(7, '2022-11-08', '5678', 'fghj', 'tryu', 'dfgh', '2022-11-21', 'fghj', 'fh'),
(8, '2022-10-16', '4567', 'xcv', 'erty', 'fghj', '2022-10-17', 'sdfgh', 'dfghj'),
(9, '2022-10-06', 'dxfgv', 'vbn', 'rt', 'gh', '2022-10-25', 'sh', 'tyu'),
(10, '2022-10-26', '4567', 'ghu', 'fg', 'dfgh', '2022-10-28', 'rdg', 'dfgh'),
(11, '2022-09-13', '567', 'hjkl', '', 'dgfhj', '2022-09-15', 'fg', 'fghj');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_srt_masuk`
--

CREATE TABLE `tb_srt_masuk` (
  `id_masuk` int(11) NOT NULL,
  `kode_masuk` varchar(20) NOT NULL,
  `pengirim` varchar(50) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `nomor_surat_masuk` varchar(20) NOT NULL,
  `tanggal_surat_masuk` date NOT NULL,
  `hal` varchar(100) NOT NULL,
  `dispo_kadis` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `file` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_srt_masuk`
--

INSERT INTO `tb_srt_masuk` (`id_masuk`, `kode_masuk`, `pengirim`, `tanggal_masuk`, `nomor_surat_masuk`, `tanggal_surat_masuk`, `hal`, `dispo_kadis`, `keterangan`, `file`) VALUES
(1, '123', 'a', '2022-01-10', 'x', '2022-01-02', 'b', 'm', 'k', 'n'),
(2, '234', 'm', '2022-01-11', '1334', '2022-01-03', 'k', 'k', 'j', 'd'),
(3, '234', 'g', '2022-02-08', '75', '2022-02-01', 'k', 'l', 'j', 'g'),
(4, 'h', 'k', '2022-02-09', '78', '2022-02-02', 'j', 'k', 'y', 'f'),
(5, '57', 'j', '2022-02-10', '57', '2022-02-03', 'k', 'h', 'm', 'v'),
(6, '68', 'n', '2022-02-11', 'g', '2022-02-04', 'd', 'fh', 'k', 'f'),
(7, '7', 'hb', '2022-03-08', '768', '2022-03-01', 'h', 'o', 'j', 'h'),
(8, '168', 'l', '2022-04-10', '78', '2022-04-01', 'j', 'hj', 'f', 'jb'),
(9, '67', 'h', '2022-04-11', '78', '2022-04-02', 'j', 'j', 'v', 'k'),
(10, '36', 'jh', '2022-04-13', '889', '2022-04-03', 'hjh', 'h', 'hj', 'o'),
(11, '58', 'q', '2022-05-09', '67', '2022-05-01', 'j', 'j', 'd', 'w'),
(12, '757', 'e', '2022-05-11', '88', '2022-05-02', 'l', 'q', 'o', 'e'),
(13, '678', 'q', '2022-06-14', '66', '2022-06-03', 'hj', 'gh', 'k', 'we'),
(14, '45', 'hj', '2022-06-16', '567', '2022-06-03', 'dfgh', 'hj', 'dfghj', 'fghj'),
(15, '4567', 'fghj', '2022-07-11', '456', '2022-07-01', 'afgh', 'ert', 'dfgh', 'adfgh'),
(16, '456', 'fgh', '2022-08-11', '456', '2022-08-01', 'gh', 'er', 'gh', 'fgh'),
(17, '34', 'gh', '2022-09-11', '32456', '2022-09-01', 's', 'ddfgh', 'dfgh', 'fgh'),
(18, '56', 'fghj', '2022-09-12', '456', '2022-09-02', 'dghj', 'ghj', 'ertyu', 'vbn'),
(19, '3456', 'fg', '2022-09-13', '456', '2022-09-03', 'ertyu', 'fgh', 'vbn', 'rf');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bulan`
--
ALTER TABLE `bulan`
  ADD PRIMARY KEY (`id_bulan`);

--
-- Indeks untuk tabel `tb_srt_keluar`
--
ALTER TABLE `tb_srt_keluar`
  ADD PRIMARY KEY (`id_keluar`);

--
-- Indeks untuk tabel `tb_srt_masuk`
--
ALTER TABLE `tb_srt_masuk`
  ADD PRIMARY KEY (`id_masuk`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bulan`
--
ALTER TABLE `bulan`
  MODIFY `id_bulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_srt_keluar`
--
ALTER TABLE `tb_srt_keluar`
  MODIFY `id_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_srt_masuk`
--
ALTER TABLE `tb_srt_masuk`
  MODIFY `id_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
